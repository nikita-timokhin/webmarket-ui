import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './user';

@Injectable({providedIn: "root"})
export class HttpUserService {

  constructor(private http: HttpClient){ }

  postData(user: User){
    return this.http.post('http://localhost:7777/auth/add', user);
  }
}
