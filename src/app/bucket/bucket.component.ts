import {Component, OnInit} from '@angular/core';
import {ProductAtBucket} from "./productAtBucket";
import {Product} from "../market/product";
import {BucketService} from "./bucketService";
import {HttpUserService} from "../registration/http.user.service";
import {User} from "../registration/user";

@Component({
  selector: 'app-bucket',
  templateUrl: './bucket.component.html',
  styleUrls: ['./bucket.component.less']
})
export class BucketComponent implements OnInit {
  productB: ProductAtBucket[] = [];
  constructor(public bucketService: BucketService) {
  }

  ngOnInit(): void {
    this.productB = this.bucketService.returnProduct();
    console.log(this.productB);
  }

  displayMin: boolean =  false;
  displayMaximizable: boolean = false;

  showMaximizableDialog() {
    this.displayMaximizable = true;
  }
  showMin(){
    this.displayMin = true;
  }
  delete(product: Product[]){
    this.bucketService.deleteProduct(product)
      .subscribe(
        (data: any) => {console.log("ok")},
        (error: any) => console.log("error")
      );
  }


}
