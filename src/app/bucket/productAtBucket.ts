import {Product} from "../market/product";

export class ProductAtBucket extends Product{
  id?:string;
  name?:string;
  price?:number;
  image?:string;
  rate?:number;
  stock?:number;
}
