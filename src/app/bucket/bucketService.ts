import {Injectable} from '@angular/core';
import {Product} from "../market/product";
import {ProductAtBucket} from "./productAtBucket";
import {HttpClient} from '@angular/common/http';

@Injectable({providedIn: "root"})
export class BucketService {
  products: Product[] = [];

  constructor(private http: HttpClient) {
  }

  setProducts(product: Product) {
    this.products.push(product);
    return this.products;
  }

  returnProduct() {
    return this.products;
  }

  order(productB: ProductAtBucket[]) {
    let i: any;
    let sum: number = 0;
    for (i of productB) {
      sum += i.price;
    }
    return sum;
  }

  deleteProduct(productB: ProductAtBucket[]) {
    let i: any;
    for (i of productB) {
     i.stock-=1;
     console.log(productB);
    }return this.http.post('http://localhost:7777/prod/reduce', productB);
  }

}
