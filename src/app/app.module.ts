import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import {CommonModule} from "@angular/common";
import { AuthenticationComponent } from './authentication/authentication.component';
import {FormsModule} from "@angular/forms";
import {ButtonModule} from "primeng/button";
import {PasswordModule} from "primeng/password";
import {InputTextModule} from "primeng/inputtext";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { RegistrationComponent } from './registration/registration.component';
import {HttpClientModule} from "@angular/common/http";
import { MarketComponent } from './market/market.component';
import {DataViewModule} from "primeng/dataview";
import {PanelModule} from "primeng/panel";
import {DialogModule} from "primeng/dialog";
import {DropdownModule} from "primeng/dropdown";
import {RippleModule} from "primeng/ripple";
import {RatingModule} from "primeng/rating";
import { BucketComponent } from './bucket/bucket.component';
import {OrderListModule} from 'primeng/orderlist';

@NgModule({
  declarations: [
    AppComponent,
    AuthenticationComponent,
    RegistrationComponent,
    MarketComponent,
    BucketComponent
  ],
  imports: [
    BrowserAnimationsModule,
    DataViewModule,
    PanelModule,
    OrderListModule,
    DialogModule,
    DropdownModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    PasswordModule,
    RippleModule,
    ButtonModule,
    RatingModule,
    InputTextModule,
    HttpClientModule,
    CommonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
