import {Component, OnInit} from '@angular/core';
import {Authentication} from "./authentication";
import {AuthenticationService} from "./authentication.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.less']
})
export class AuthenticationComponent implements OnInit {
  authentication = new Authentication("","");

  constructor(private authenticationService: AuthenticationService,private route:ActivatedRoute,private router:Router) {
  }

  submitAuth(authentication: Authentication) {
    this.authenticationService.validateAuth(authentication)
      .subscribe(
        (data: any) => {
            this.router.navigateByUrl('market');
          },error => {
        console.log("error verification");}
      );
  }

  ngOnInit(): void {
  }
}
