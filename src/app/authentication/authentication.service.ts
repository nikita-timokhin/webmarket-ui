import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Authentication} from "./authentication";

@Injectable({providedIn: "root"})
export class AuthenticationService {

  constructor(private http: HttpClient){ }

  validateAuth(authentication: Authentication){
    return this.http.post('http://localhost:7777/auth/verify', authentication);
  }
}
