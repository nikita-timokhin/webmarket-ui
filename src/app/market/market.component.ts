import { Component, OnInit } from '@angular/core';
import{ProductService} from "./product.service";
import { Product } from './product';
import {SelectItem} from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';
import {BucketService} from "../bucket/bucketService";

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.less']
})
export class MarketComponent implements OnInit {
  products:  Product[] = [];
    sortOptions: SelectItem[] = [];

  sortOrder: number = 0;

  sortField: string = "" ;
  sortKey: any;

  constructor(public productService: ProductService, private primengConfig: PrimeNGConfig, public bucketService: BucketService) { }


  ngOnInit(): void {
    this.productService.getProducts().subscribe(data => {this.products = data;} );

    this.sortOptions = [
      {label: 'Price High to Low', value: '!price'},
      {label: 'Price Low to High', value: 'price'},
      {label: 'Rate High to Low', value: '!rate'},
      {label: 'Rate Low to High', value: 'rate'},
    ];

    this.primengConfig.ripple = true;
  }

  onSortChange(event: any) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    }
    else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }
}
